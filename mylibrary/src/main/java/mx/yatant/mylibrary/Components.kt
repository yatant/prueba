package mx.yatant.mylibrary

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.input.pointer.PointerIconDefaults.Text
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Greeting("Android")
}